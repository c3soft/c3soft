const media = document.querySelectorAll('.media');
const title = document.querySelector('.title');

window.addEventListener('load', () =>{
const tl = gsap.timeline({paused: true});
  tl
  .staggerFrom(title, 1, {
    transform: "scale(0)",
    ease: "power2.out"
  }, '-=2')
    
  .staggerFrom(media, 0.5, {
    right: -200,
    ease: "power2.out"
  }, 0.3, '-=1')


tl.play();
})

// Navbar personnalisé responsive
// ---------------------------------------------------------------------------

const btnNav = document.getElementById('btnNav');
const mainNavbar = document.getElementById('mainNavbar');
const navbarTogglerDemo02 = document.getElementById('navbarTogglerDemo02');


btnNav.addEventListener('click',() => {
btnNav.classList.toggle('active');
mainNavbar.classList.toggle('js-mainNavbar');
navbarTogglerDemo02.classList.toggle('js-collapseNavbar');
})

//  home text animation
// ---------------------------------------------------------------------------

// setTimeout(function(){
//     var textWrapper = document.querySelector('.ml7 .letters');
//     textWrapper.innerHTML = "Développeur web".replace(/\S/g, "<span class='letter'>$&</span>");
    
//     anime.timeline({loop: false})
//       .add({
//         opacity: 1,
//         targets: '.ml7 .letter',
//         translateY: ["1.1em", 0],
//         translateX: ["0.55em", 0],
//         translateZ: 0,
//         rotateZ: [180, 0],
//         duration: 750,
//         easing: "easeOutExpo",
//         delay: (el, i) => 100 * i
//       })
// }, 3500);

// Toast contact
// ---------------------------------------------------------------------------
var toastElList = [].slice.call(document.querySelectorAll('.toast'))
var toastList = toastElList.map(function (toastEl) {
  return new bootstrap.Toast(toastEl)
})

toastList.forEach(toast => toast.show());