//  home text animation
// ---------------------------------------------------------------------------

setTimeout(function () {
    var textWrapper = document.querySelector('.ml7 .letters');
    textWrapper.innerHTML = "Développeur web".replace(/\S/g, "<span class='letter'>$&</span>");
    
    anime.timeline({loop: false})
      .add({
        opacity: 1,
        targets: '.ml7 .letter',
        translateY: ["1.1em", 0],
        translateX: ["0.55em", 0],
        translateZ: 0,
        rotateZ: [180, 0],
        duration: 750,
        easing: "easeOutExpo",
        delay: (el, i) => 100 * i
      })
}, 3500);