const ww = window.innerWidth ;
const hh = window.innerHeight ;

// Create the Three.js Scene
const scene = new THREE.Scene();

// Create a new perspective Camera
const camera = new THREE.PerspectiveCamera( 20, ww / hh, 0.1, 1000 );
camera.position.z = 105;

const canvas = document.querySelector('#c');
const renderer = new THREE.WebGLRenderer({canvas, alpha: true});

// Create a full screen WebGL renderer
renderer.setSize( ww, hh );

document.body.appendChild( renderer.domElement );

const light = new THREE.PointLight(0xFFFFFF, 1.4, 1000);
light.position.set(0,15,15);
scene.add( light );   

// Create a material
let ourObj;
const mtlLoader = new THREE.MTLLoader();
mtlLoader.load('../images/3d/c.mtl', function (materials){

    materials.preload();

    // Load the project
    const objLoader = new THREE.OBJLoader();
    objLoader.setMaterials(materials);
    objLoader.load('../images/3d/c.obj', function (object){
        scene.add(object);  
        ourObj = object;
        object.position.z -= 90;
        object.position.x = 0
        object.position.y = 10

        const tl = gsap.timeline();
        tl.from(ourObj.scale, 2, {y: 30, x:0, z: 0, ease: Expo.easeOut})            
                    
        animate();
    })
});


const animate = function () {
    requestAnimationFrame(animate);

    // Rotate the object indifinitely
     ourObj.rotation.z += .01;
     ourObj.rotation.y += 0.01;

    renderer.render(scene, camera);
}
